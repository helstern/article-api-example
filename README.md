article-api
========

Overview
---

 - Simple API that allows to create an article, answer to an article, rate an article (between 0 and 5 ==> 0, 1, 2, 3, 4, 5)
 - The API should retrieves an article and all its answers

Installation
----

the project requires virtualbox and vagrant

on the host machine, cd into the dir of the project and run 
    
    make vagrant

this will download the scotch.io box, install and compile the extension php5-inotify and start a new virtual machine.

after bootstrap, still on the host machine run
    
    cd vagrant; vagrant ssh
    
you are now inside the virtual machine and you must install the app. the app is located at /var/www
    
    cd /var/www; make app 
    
    
Architecture
----

The application exposes a JSON API backed by a MYSQL Database. It uses a pretty standard Symfony3 stack, wich includes
Doctrine2 as the persistance layer. The aim was to end up with a layered architecture

At the core of the application lies it's DomainModel, with it's role of defining the entities and business logic.
The Persistance Model, here represented by Doctrine2 entities is an implementation of the DomainModel.

The Domain Model's entitites also map 1:1 on the resources exposed by the api

The Persistance Model is manipulated and interrogated by the Application Services layer, which has one application service per
aggregate root. in this case the aggregate roots map 1:1 to entities. Because the scope of the application was small
I applied the Segregated Interface pattern with the DoctrineService which implements behaviour from three distinct client interface

The Controllers communicate with the Application Service in order to create or retrieve new Domain Entities. 
They also map Domain Entities to Resource Representations which are then serialized using JMSBundle annotations and the sent over the wire
using and ObjectMapper. In this way the domain and the representation are kept separate and clean

The resource representations do not map 1:1 to the number of resources, there are cases when a resource uses more then one depending on the data required for its endpoin 

To conclude, the advantage of this layered approach is that it keeps the components separate and lean and also ensures 
maximum flexibility which will be needed when and if the api representations and the domain model will diverge.
