SHELL=/bin/bash

app: download composer database

init: download

vagrant:
	cd vagrant; vagrant up

download:
	if ! test -d bin; then mkdir bin; fi
	rm -rf bin/symfony; curl -LsS https://symfony.com/installer -o bin/symfony && chmod a+x bin/symfony
	rm -rf bin/composer.phar; curl -sS https://getcomposer.org/composer.phar -o bin/composer.phar && chmod a+x bin/composer.phar

composer:
	php bin/composer.phar install

database:
	php bin/console doctrine:database:create
	php bin/console doctrine:schema:create

clean:
	php bin/console doctrine:database:drop --force
	rm -rf vendor
	rm -rf var/cache/dev/*
	rm -rf var/cache/test/*
	rm -rf var/cache/prod/*
