<?php namespace AppBundle\Service;

use AppBundle\DomainModel\Article;

interface ArticleService
{
    /**
     * @param string $author
     * @param string $content
     *
     * @return \AppBundle\DomainModel\Article
     */
    public function createArticle($author, $content);

    /**
     * @param string $articleId
     *
     * @throws ArticleNotFoundException
     * @return boolean
     */
    function verifyArticleExists($articleId);

    /**
     * @param string $articleId
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    function findArticle($articleId);

    /**
     * @param string $articleId
     *
     * @return Article
     * @throws ArticleNotFoundException
     */
    function findArticleWithAnswers($articleId);
}
