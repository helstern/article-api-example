<?php namespace AppBundle\Service;

use AppBundle\DomainModel\Rating;
use AppBundle\Entity\RatingEntity;

interface RatingService
{
    /**
     * @param $articleId
     * @param $ratingId
     *
     * @throws ArticleNotFoundException
     * @throws RatingNotFoundException
     * @return Rating
     */
    function findArticleRating($articleId, $ratingId);

    /**
     * @param $articleId
     * @param $score
     *
     * @throws UnsupportedRatingException
     * @throws ArticleNotFoundException
     * @return RatingEntity
     *
     */
    function rateArticle($articleId, $score);


}
