<?php namespace AppBundle\Service;

class AnswerNotFoundException extends \Exception
{
    public function __construct($code = 0, \Exception $previous = null)
    {
        $message = 'Answer not found';
        parent::__construct($message, $code, $previous);
    }
}
