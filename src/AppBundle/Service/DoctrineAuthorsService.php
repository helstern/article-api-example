<?php namespace AppBundle\Service;


use AppBundle\Entity\ArticleEntity;
use AppBundle\Entity\AuthorEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class DoctrineAuthorsService implements AuthorsService
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $authorClass;

    public function __construct(
        EntityManager $em,
        $authorClass
    ) {
        $this->em = $em;
        $this->authorClass = $authorClass;
    }

    /**
     * @param $authorName
     * @param int $hoursAgo
     *
     * @return \AppBundle\Entity\AuthorEntity[]
     */
    function findOneNotifiedSinceByName($authorName, $hoursAgo)
    {
        $ago = new \DateTime();
        $ago->modify("-$hoursAgo hours");

        $qb = $this->em->getRepository($this->authorClass)->createQueryBuilder('a');
        $query = $qb->select('a, b')
            ->innerJoin('a.articles', 'b')
            ->innerJoin('b.answers', 'c')
            ->where('c.dateAdded >= :articleId')
            ->andWhere('a.name = :name')
            ->setParameter('dateAdded', $ago)
            ->setParameter('name', $authorName)
            ->getQuery();

        try {
            /** @var ArticleEntity $result */
            $article = $query->getSingleResult();
            return $article;
        }  catch(NonUniqueResultException $e) {
            return null;
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param int $hoursAgo
     *
     * @return AuthorEntity[]
     */
    function findAllNotifiedSince($hoursAgo)
    {
        $ago = new \DateTime();
        $ago->modify("-$hoursAgo hours");

        $qb = $this->em->getRepository($this->authorClass)->createQueryBuilder('a');
        $query = $qb->select('a, b')
            ->innerJoin('a.articles', 'b')
            ->innerJoin('b.answers', 'c')
            ->where('c.dateAdded >= :articleId')
            ->setParameter('dateAdded', $ago)
            ->getQuery();

            return $query->getResult();
    }
}
