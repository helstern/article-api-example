<?php namespace AppBundle\Service;

use AppBundle\DomainModel\Answer;

interface AnswerService
{
    /**
     * @param $articleId
     * @param $answerId
     *
     * @return Answer
     */
    function findArticleAnswer($articleId, $answerId);

    /**
     * @param $articleId
     * @param $answer
     *
     * @return Answer
     */
    function answerToArticle($articleId, $answer);
}
