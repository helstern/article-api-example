<?php namespace AppBundle\Service;

class UnsupportedRatingException extends \Exception
{
    public function __construct($code = 0, \Exception $previous = null)
    {
        $message = 'Unsupported rating';
        parent::__construct($message, $code, $previous);
    }
}
