<?php namespace AppBundle\Service;

class ArticleNotFoundException extends \Exception
{
    public function __construct($code = 0, \Exception $previous = null)
    {
        $message = 'Article not found';
        parent::__construct($message, $code, $previous);
    }
}
