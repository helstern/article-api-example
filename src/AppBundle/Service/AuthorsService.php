<?php namespace AppBundle\Service;


use AppBundle\DomainModel\Author;

interface AuthorsService
{
    /**
     * @param $authorName
     * @param int $hoursAgo
     *
     * @return Author
     */
    function findOneNotifiedSinceByName($authorName, $hoursAgo);

    /**
     * @param $hoursAgo
     *
     * @return Author
     */
    function findAllNotifiedSince($hoursAgo);
}
