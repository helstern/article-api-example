<?php namespace AppBundle\Service;

class RatingNotFoundException extends \Exception
{
    public function __construct($code = 0, \Exception $previous = null)
    {
        $message = 'Rating not found';
        parent::__construct($message, $code, $previous);
    }
}
