<?php namespace AppBundle\Service;

use AppBundle\DomainModel\ArticleRatingPolicy;
use AppBundle\Entity\AnswerEntity;
use AppBundle\Entity\ArticleEntity;
use AppBundle\Entity\AuthorEntity;
use AppBundle\Entity\RatingEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;

class DoctrineService implements AnswerService, ArticleService, RatingService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $articleClass;

    /**
     * @var string
     */
    private $answerClass;

    /**
     * @var string
     */
    private $ratingClass;

    /**
     * @var ArticleRatingPolicy
     */
    private $articleRatingPolicy;


    public function __construct(
        EntityManager $em,
        $articleClass,
        $answerClass,
        $ratingClass,
        $authorClass,
        ArticleRatingPolicy $articleRatingPolicy
    ) {
        $this->em = $em;
        $this->articleClass = $articleClass;
        $this->answerClass = $answerClass;
        $this->ratingClass = $ratingClass;
        $this->authorClass = $authorClass;
        $this->articleRatingPolicy = $articleRatingPolicy;
    }

    /**
     * @param $articleId
     * @param $answer
     *
     * @return \AppBundle\Entity\AnswerEntity
     * @throws ArticleNotFoundException
     * @throws \Exception
     */
    function answerToArticle($articleId, $answer)
    {
        $entity = new AnswerEntity();
        $entity->setContent($answer);
        $entity->setDateAdded(new \DateTime());


        $article = $this->findArticle($articleId);
        $entity->setArticle($article);
        $this->persistEntity($entity);

        return $entity;
    }

    /**
     * @param string $articleId
     *
     * @return bool
     */
    function verifyArticleExists($articleId)
    {
        // this method should not hydrate a new entity, it should use lower level objects, like queries
        // to be more efficient
        $article = $this->em->getRepository($this->articleClass)->find($articleId);
        return !is_null($article);
    }

    /**
     * @param string $articleId
     *
     * @return ArticleEntity
     * @throws ArticleNotFoundException
     */
    function findArticle($articleId)
    {
        $article = $this->em->getRepository($this->articleClass)->find($articleId);
        if (is_null($article)) {
            throw new ArticleNotFoundException();
        }

        return $article;
    }

    /**
     * @param string $articleId
     *
     * @return ArticleEntity
     * @throws ArticleNotFoundException
     */
    function findArticleWithAnswers($articleId)
    {
        $qb = $this->em->getRepository($this->articleClass)->createQueryBuilder('a');
        $query = $qb->select('a, b')
            ->leftJoin('a.answers', 'b')
            ->where('a.id = :articleId')
            ->setParameter('articleId', $articleId)
            ->getQuery()
        ;

        try {
            /** @var ArticleEntity $result */
            $article = $query->getSingleResult();
        }  catch(NonUniqueResultException $e) {
            throw new ArticleNotFoundException();
        } catch (NoResultException $e) {
            throw new ArticleNotFoundException();
        }

        return $article;
    }

    /**
     * @param string $authorName
     * @param string $content
     *
     * @return \AppBundle\Entity\ArticleEntity
     * @throws \Exception
     * @internal param string $author
     */
    public function createArticle($authorName, $content)
    {
        $article = new ArticleEntity();

        $author = $this->em->getRepository($this->authorClass)->findOneBy(array('name' => $authorName));
        if (is_null($author)) {
            $author = new AuthorEntity();
            $author->setName($authorName);
        }
        $article->setAuthor($author);
        $article->setContent($content);

        $this->persistEntity($article);

        return $article;
    }

    /**
     * @param $articleId
     * @param $answerId
     *
     * @return AnswerEntity
     * @throws AnswerNotFoundException
     * @throws ArticleNotFoundException
     */
    function findArticleAnswer($articleId, $answerId)
    {
        $childEntity = $this->em->getRepository($this->answerClass)->findOneBy(array(
            'article' => $articleId,
            'id' => $answerId
        ));

        if (! is_null($childEntity)) {
            return $childEntity;
        }

        //find article
        if ($this->verifyArticleExists($articleId)) {
            throw new AnswerNotFoundException();
        } else {
            throw new ArticleNotFoundException();
        }
    }

    /**
     * @param $articleId
     * @param $ratingId
     *
     * @return RatingEntity
     * @throws AnswerNotFoundException
     * @throws ArticleNotFoundException
     */
    function findArticleRating($articleId, $ratingId)
    {
        $childEntity = $this->em->getRepository($this->ratingClass)->findOneBy([
            'article' => $articleId,
            'id' => $ratingId
        ]);

        if (! is_null($childEntity)) {
            return $childEntity;
        }

        //find article
        if ($this->verifyArticleExists($articleId)) {
            throw new AnswerNotFoundException();
        } else {
            throw new ArticleNotFoundException();
        }
    }


    /**
     * @param $articleId
     * @param $score
     *
     * @return RatingEntity
     * @throws ArticleNotFoundException
     * @throws UnsupportedRatingException
     * @throws \Exception
     */
    function rateArticle($articleId, $score)
    {
        $rating = new RatingEntity();
        $rating->setScore($score);

        $article = $this->findArticle($articleId);
        $rating->setArticle($article);

        if ($this->articleRatingPolicy->allowRating($rating)) {
            $this->persistEntity($rating);
            return $rating;
        }

        throw new UnsupportedRatingException();
    }

    private function persistEntity($entity)
    {
        $this->em->getConnection()->beginTransaction(); // suspend auto-commit
        try {
            $this->em->persist($entity);
            $this->em->flush($entity);
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }
    }
}
