<?php namespace AppBundle\Entity;

use AppBundle\DomainModel\Article;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ArticleEntity
 *
 * @package AppBundle\Entity
 * @ORM\Table(name="article")
 * @ORM\Entity()
 */
class ArticleEntity implements Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var AnswerEntity[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AnswerEntity", mappedBy="article")
     */
    private $answers;

    /**
     * @var AuthorEntity
     *
     * @ORM\ManyToOne(targetEntity="AuthorEntity", cascade={"persist"})
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param mixed $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * @return AuthorEntity
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param AuthorEntity $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }
}
