<?php namespace AppBundle\Entity;

use AppBundle\DomainModel\Answer;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Answer
 *
 * @package AppBundle\Entity
 * @ORM\Table(name="answer")
 * @ORM\Entity()
 */
class AnswerEntity implements Answer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime", nullable=false)
     */
    private $dateAdded;

    /**
     * @var ArticleEntity
     *
     * @ORM\ManyToOne(targetEntity="ArticleEntity")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @return string
     */
    function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArticleEntity
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param ArticleEntity $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @param \DateTime $dateAdded
     */
    public function setDateAdded(\DateTime $dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }
}
