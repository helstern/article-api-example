<?php namespace AppBundle\Entity;

use AppBundle\DomainModel\Rating;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ArticleEntity
 *
 * @package AppBundle\Entity
 * @ORM\Table(name="rating")
 * @ORM\Entity()
 */
class RatingEntity implements Rating
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer", length=2, nullable=false)
     */
    private $score;

    /**
     * @var ArticleEntity
     *
     * @ORM\ManyToOne(targetEntity="ArticleEntity")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     * @return string
     */
    function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    function getScore()
    {
        return $this->score;
    }

    /**
     * @param $score
     *
     */
    function setScore($score)
    {
        $this->score = $score;
    }

    /**
     * @return ArticleEntity
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param ArticleEntity $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }
}
