<?php namespace AppBundle\Entity;

use AppBundle\DomainModel\Article;
use AppBundle\DomainModel\Author;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AuthorEntity
 *
 * @package AppBundle\Entity
 * @ORM\Table(
 *  name="author",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="name_idx",columns={"name"})},
 * )
 * @ORM\Entity()
 */
class AuthorEntity implements Author
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var ArticleEntity[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="ArticleEntity", mappedBy="author")
     */
    private $articles;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArticleEntity[]|ArrayCollection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param ArticleEntity[]|ArrayCollection $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }

    /**
     * @return string
     */
    function getEmailAddress()
    {
        return 'test@localhost';
    }
}
