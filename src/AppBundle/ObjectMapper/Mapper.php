<?php namespace AppBundle\ObjectMapper;

use AppBundle\DomainModel\Answer;
use AppBundle\DomainModel\Article;
use AppBundle\DomainModel\Rating;
use AppBundle\Representation\Json\AnswerRepresentation;
use AppBundle\Representation\Json\ArticleRepresentation;
use AppBundle\Representation\Json\RatingRepresentation;

class Mapper
{
    public static function toRatingRepresentation(Rating $source, RatingRepresentation $target)
    {
        $id = $source->getId();
        $target->setId($id);

        $score = $source->getScore();
        $target->setScore($score);
    }

    public static function toAnswerRepresentation(Answer $source, AnswerRepresentation $target)
    {
        $id = $source->getId();
        $target->setId($id);

        $content = $source->getContent();
        $target->setContent($content);
    }

    public static function toArticleRepresentation(Article $source, ArticleRepresentation $target)
    {
        $id = $source->getId();
        $target->setId($id);

        $content = $source->getContent();
        $target->setContent($content);

        $answersRepresentations = [];
        foreach ($source->getAnswers() as $answer) {
            $representation =  new AnswerRepresentation();
            self::toAnswerRepresentation($answer, $representation);
            $answersRepresentations[] = $representation;
        }
        $target->setAnswers($answersRepresentations);
    }
}
