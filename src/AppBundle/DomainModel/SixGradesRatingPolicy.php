<?php namespace AppBundle\DomainModel;

class SixGradesRatingPolicy implements ArticleRatingPolicy
{
    /**
     * @var int
     */
    protected $minScore = 0;

    /**
     * @var int
     */
    protected $maxScore = 5;

    /**
     * @param \AppBundle\DomainModel\Rating $rating
     *
     * @return bool
     */
    function allowRating(Rating $rating)
    {
        $score = $rating->getScore();
        if ($this->minScore <= $score && $score <= $this->maxScore) {
            return true;
        }

        return false;
    }
}
