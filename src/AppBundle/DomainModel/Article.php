<?php namespace AppBundle\DomainModel;

interface Article
{
    /**
     * @return string
     */
    function getId();

    /**
     * @return string
     */
    function getContent();

    /**
     * @return Answer[]
     */
    function getAnswers();
}
