<?php namespace AppBundle\DomainModel;

interface ArticleRatingPolicy
{
    /**
     * @param Rating $rating
     *
     * @return bool
     */
    function allowRating(Rating $rating);
}
