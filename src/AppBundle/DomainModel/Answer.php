<?php namespace AppBundle\DomainModel;

interface Answer
{
    /**
     * @return string
     */
    function getId();

    /**
     * @return string
     */
    function getContent();
}
