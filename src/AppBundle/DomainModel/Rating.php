<?php namespace AppBundle\DomainModel;

interface Rating
{
    /**
     * @return string
     */
    function getId();

    /**
     * @return int
     */
    function getScore();
}
