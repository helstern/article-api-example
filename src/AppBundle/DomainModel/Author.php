<?php namespace AppBundle\DomainModel;

interface Author
{
    /**
     * @return string
     */
    function getId();

    /**
     * @return string
     */
    function getName();

    /**
     * @return string
     */
    function getEmailAddress();
}
