<?php namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class CreateResourceResponseFactory
{
    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param array $resourceNames
     * @param array $routeParams
     * @param $contentType
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createForSubResource(array $resourceNames, array $routeParams, $contentType)
    {
        $routeName = sprintf('get_%s', implode('_', $resourceNames));
        $routeName = strtolower($routeName);

        return $this->create($routeName, $routeParams, $contentType);
    }

    /**
     * @param $resourceName
     * @param array $routeParams
     * @param $contentType
     *
     * @return Response
     */
    public function createForResource($resourceName, array $routeParams, $contentType)
    {
        $routeName = sprintf('get_%s', $resourceName);
        $routeName = strtolower($routeName);

        return $this->create($routeName, $routeParams, $contentType);
    }

    /**
     * @param $routeName
     * @param array $routeParams
     * @param $contentType
     *
     * @return Response
     */
    public function create($routeName, array $routeParams, $contentType)
    {
        $url = $this->router->generate($routeName, $routeParams);
        $response = new Response('', 302, [
            'Location' => $url,
            'Content-Type' => $contentType
        ]);

        return $response;
    }
}
