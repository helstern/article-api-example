<?php namespace AppBundle\Controller;

use AppBundle\ObjectMapper\Mapper;
use AppBundle\Representation\Json\AnswerRepresentation;
use AppBundle\Representation\Json\TextBodyRepresentation;
use AppBundle\Service\AnswerService;
use AppBundle\Service\ArticleNotFoundException;
use AppBundle\Service\ArticleService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @FOSRest\RouteResource("Answer")
 *
 * @package AppBundle\Controller
 */
class AnswerController
{
    /**
     * @var AnswerService
     */
    protected $answerService;

    /**
     * @var CreateResourceResponseFactory
     */
    private $createResponseFactory;

    public function __construct(AnswerService $answerService, CreateResourceResponseFactory $createResponseFactory)
    {
        $this->answerService = $answerService;
        $this->createResponseFactory = $createResponseFactory;
    }

    public function getAction($articleId, $answerId)
    {
        try {
            $answer = $this->answerService->findArticleAnswer($articleId, $answerId);
        } catch (ArticleNotFoundException $e) {
            throw $e;
        }

        $representation = new AnswerRepresentation();
        Mapper::toAnswerRepresentation($answer, $representation);

        return $representation;
    }

    /**
     * @FOSRest\Post("/articles/{articleId}/answers")
     * @ParamConverter("answer", converter="fos_rest.request_body", class="AppBundle\Representation\Json\TextBodyRepresentation")
     * @param $articleId
     * @param \AppBundle\Representation\Json\TextBodyRepresentation $answer
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction($articleId, TextBodyRepresentation $answer, Request $request)
    {
        $answerText = $answer->getContent();
        $answer = $this->answerService->answerToArticle($articleId, $answerText);

        $response = $this->createResponseFactory->createForSubResource(
            ['article','answer'],
            ['articleId' => $articleId, 'answerId' => $answer->getId()],
            $request->headers->get('CONTENT_TYPE')
        );

        return $response;
    }
}
