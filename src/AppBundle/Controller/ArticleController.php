<?php

namespace AppBundle\Controller;

use AppBundle\ObjectMapper\Mapper;
use AppBundle\Representation\Json\ArticleRepresentation;
use AppBundle\Representation\Json\ArticleSubmissionRepresentation;
use AppBundle\Representation\Json\TextBodyRepresentation;
use AppBundle\Service\ArticleNotFoundException;
use AppBundle\Service\ArticleService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as FOSRest;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\Response;


/**
 * @FOSRest\RouteResource("Article")
 *
 * @package AppBundle\Controller
 */
class ArticleController
{
    /**
     * @var ArticleService
     */
    protected $articleService;

    /**
     * @var CreateResourceResponseFactory
     */
    private $createResponseFactory;

    public function __construct(ArticleService $articleService, CreateResourceResponseFactory $createResponseFactory)
    {
        $this->articleService = $articleService;
        $this->createResponseFactory = $createResponseFactory;
    }

    /**
     * @param string $articleId
     *
     * @return \AppBundle\Representation\Json\ArticleRepresentation
     * @throws \AppBundle\Service\ArticleNotFoundException
     * @throws \Exception
     */
    public function getAction($articleId)
    {
        $article = $this->articleService->findArticleWithAnswers($articleId);
        $representation = new ArticleRepresentation();
        Mapper::toArticleRepresentation($article, $representation);

        return $representation;
    }

    /**
     * @FOSRest\Post("/articles")
     * @ParamConverter("submission", converter="fos_rest.request_body")
     * @param \AppBundle\Representation\Json\ArticleSubmissionRepresentation $submission
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction(ArticleSubmissionRepresentation $submission, Request $request)
    {
        $author = $submission->getAuthor();
        $content = $submission->getContent();

        $article = $this->articleService->createArticle($author, $content);

        $response = $this->createResponseFactory->createForResource(
            'article',
            array('articleId' => $article->getId()),
            $request->headers->get('CONTENT_TYPE')
        );

        return $response;
    }
}
