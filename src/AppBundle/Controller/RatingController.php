<?php

namespace AppBundle\Controller;

use AppBundle\ObjectMapper\Mapper;
use AppBundle\Representation\Json\RatingRepresentation;
use AppBundle\Representation\Json\ScoreRepresentation;
use AppBundle\Service\ArticleNotFoundException;
use AppBundle\Service\RatingNotFoundException;
use AppBundle\Service\RatingService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\Routing\RouterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @FOSRest\RouteResource("Rating")
 *
 * @package AppBundle\Controller
 */
class RatingController
{
    /**
     * @var RatingService
     */
    private $ratingService;

    /**
     * @var CreateResourceResponseFactory
     */
    private $createResponseFactory;

    public function __construct(RatingService $ratingService, CreateResourceResponseFactory $createResponseFactory)
    {
        $this->ratingService = $ratingService;
        $this->createResponseFactory = $createResponseFactory;
    }

    public function getAction($articleId, $ratingId)
    {
        $rating = $this->ratingService->findArticleRating($articleId, $ratingId);
        $representation = new RatingRepresentation();
        Mapper::toRatingRepresentation($rating,  $representation);

        return $representation;
    }

    /**
     * @FOSRest\Post("/articles/{articleId}/ratings")
     * @ParamConverter("representation", converter="fos_rest.request_body" )
     * @param $articleId
     * @param ScoreRepresentation $representation
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAction($articleId, ScoreRepresentation $representation, Request $request)
    {
        $score = $representation->getScore();
        $rating = $this->ratingService->rateArticle($articleId, $score);

        $response = $this->createResponseFactory->createForSubResource(
            ['article','rating'],
            ['articleId' => $articleId, 'ratingId' => $rating->getId()],
            $request->headers->get('CONTENT_TYPE')
        );

        return $response;
    }
}
