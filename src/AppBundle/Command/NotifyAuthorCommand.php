<?php namespace AppBundle\Command;

use AppBundle\DomainModel\Author;
use AppBundle\Service\AuthorsService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyAuthorCommand extends ContainerAwareCommand
{
    /**
     * @var AuthorsService
     */
    protected $authorsService;

    /**
     * @var int
     */
    protected $notificationPeriod = 24;

    /**
     * @param \AppBundle\Service\AuthorsService $authorsService
     * @param null $name
     *
     */
    public function __construct(AuthorsService $authorsService, $name = null)
    {
        parent::__construct($name);
        $this->authorsService = $authorsService;
    }

    protected function configure()
    {
        $this
            ->setName('author:notify')
            ->setDescription('emails an author if he/she had any notifications in the last 24 hours')
            ->addOption(
                'name',
                null,
                InputOption::VALUE_REQUIRED,
                'the name of the article author'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getOption('name');
        if (empty($name)) {
            $output->writeln('option --name must be present');
            return -1;
        }

        $author = $this->authorsService->findOneNotifiedSinceByName($name, $this->notificationPeriod);
        if (is_null($author)) {
            $output->writeln('Author not found or no new notifications found ');
            return -1;
        }

        $message = $this->configureMessage($author);
        $this->getContainer()->get('mailer')->send($message);

        $output->writeln('Notified author via email!');
        return 0;
    }

    protected function configureMessage(Author $forAuthor)
    {
        $authorEmail = $forAuthor->getEmailAddress();

        $message = \Swift_Message::newInstance()
            ->setSubject('Notifications since 24 hrs ago')
            ->setFrom('iron-web@iron-web.com')
            ->setTo($authorEmail)
            ->setCharset('UTF-8')
            ->setContentType('text/html')
            ->setBody('');

        return $message;
    }
}
