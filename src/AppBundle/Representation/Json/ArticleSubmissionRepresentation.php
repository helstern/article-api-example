<?php namespace AppBundle\Representation\Json;

use JMS\Serializer\Annotation as JMS;

class ArticleSubmissionRepresentation
{
    /**
     * @var string
     *
     * @JMS\SerializedName("author")
     * @JMS\Type("string")
     */
    private $author;

    /**
     * @var
     *
     * @JMS\SerializedName("content")
     * @JMS\Type("string")
     */
    private $content;

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return string
     */
    function getContent()
    {
        return $this->content;
    }
}
