<?php namespace AppBundle\Representation\Json;

use JMS\Serializer\Annotation as JMS;

class RatingRepresentation
{
    /**
     * @var string
     *
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     */
    private $id;


    /**
     * @var int
     *
     * @JMS\SerializedName("score")
     * @JMS\Type("integer")
     */
    private $score;

    /**
     * @return string
     */
    function getScore()
    {
        return $this->score;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param int $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

}
