<?php namespace AppBundle\Representation\Json;

use JMS\Serializer\Annotation as JMS;

class AnswerRepresentation
{
    /**
     * @var string
     *
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     */
    private $id;

    /**
     * @var string
     *
     * @JMS\SerializedName("content")
     * @JMS\Type("string")
     */
    private $content;

    /**
     * @return string
     */
    function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    function setContent($content)
    {
        $this->content = $content;
    }

}
