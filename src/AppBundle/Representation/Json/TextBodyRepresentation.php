<?php namespace AppBundle\Representation\Json;

use JMS\Serializer\Annotation as JMS;

class TextBodyRepresentation
{
    /**
     * @var
     *
     * @JMS\SerializedName("content")
     * @JMS\Type("string")
     */
    private $content;

    /**
     * @return string
     */
    function getContent()
    {
        return $this->content;
    }

}
