<?php namespace AppBundle\Representation\Json;

use JMS\Serializer\Annotation as JMS;

class ArticleRepresentation
{
    /**
     * @var string
     *
     * @JMS\SerializedName("id")
     * @JMS\Type("string")
     */
    private $id;

    /**
     * @var string
     *
     * @JMS\SerializedName("author")
     * @JMS\Type("string")
     */
    private $author;

    /**
     * @var string
     *
     * @JMS\SerializedName("content")
     * @JMS\Type("string")
     */
    private $content;

    /**
     * @var AnswerRepresentation[]
     *
     * @JMS\SerializedName("answers")
     * @JMS\Type("array<AppBundle\Representation\Json\AnswerRepresentation>")
     */
    private $answers;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return AnswerRepresentation[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param AnswerRepresentation[] $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }
}
