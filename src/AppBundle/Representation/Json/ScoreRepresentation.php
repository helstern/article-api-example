<?php namespace AppBundle\Representation\Json;

use JMS\Serializer\Annotation as JMS;

class ScoreRepresentation
{
    /**
     * @var int
     *
     * @JMS\SerializedName("score")
     * @JMS\Type("integer")
     */
    private $score;

    /**
     * @return string
     */
    function getScore()
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }

}
