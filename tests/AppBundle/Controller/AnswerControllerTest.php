<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AnswerControllerTest extends WebTestCase
{
    public function testAnswerArticleResponse()
    {
        $postHeaders = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
        ];
        $getHeaders = ['HTTP_ACCEPT' => 'application/json'];
        $client = static::createClient();


        $content = <<<EOF
{"content": "article content", "author": "author1"}
EOF;
        $client->request('POST', '/api/v1/articles', [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $location = $client->getResponse()->headers->get('location');
        $client->request('GET', $location, [], [], $getHeaders);
        $articleId = json_decode($client->getResponse()->getContent(), true)['id'];

        $content = <<<EOF
{"content": "answer"}
EOF;
        $client->request('POST', "/api/v1/articles/$articleId/answers", [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode(), 'failed to create an answer');

        $location = $client->getResponse()->headers->get('location');
        $client->request('GET', $location, [], [], $getHeaders, $content);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testArticleNotFoundResponse()
    {
        $postHeaders = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
        ];

        $getHeaders = ['HTTP_ACCEPT' => 'application/json'];


        $client = static::createClient();

        $client->request('GET', "/api/v1/articles/7777", [], [], $getHeaders);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());


        $content = <<<EOF
{"content": "some content"}
EOF;
        $client->request('POST', "/api/v1/articles/7777/answers", [], [], $postHeaders, $content);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }



}
