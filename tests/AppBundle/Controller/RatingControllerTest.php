<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RatingControllerTest extends WebTestCase
{
    public function testRateArticleResponse()
    {
        $postHeaders = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
        ];

        $getHeaders = ['HTTP_ACCEPT' => 'application/json'];
        $client = static::createClient();


        $content = <<<EOF
{"content": "article content", "author": "author1"}
EOF;
        $client->request('POST', '/api/v1/articles', [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $location = $client->getResponse()->headers->get('location');
        $client->request('GET', $location, [], [], $getHeaders, $content);
        $articleId = json_decode($client->getResponse()->getContent(), true)['id'];

        $content = <<<EOF
{"score": 4}
EOF;
        $client->request('POST', "/api/v1/articles/$articleId/ratings", [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $location = $client->getResponse()->headers->get('location');

        $client->request('GET', $location, [], [], $getHeaders, $content);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testUnsupportedRatingResponse()
    {
        $postHeaders = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
        ];

        $getHeaders = ['HTTP_ACCEPT' => 'application/json'];


        $client = static::createClient();


        $content = <<<EOF
{"content": "article content", "author": "author1"}
EOF;
        $client->request('POST', '/api/v1/articles', [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $articleLocation = $client->getResponse()->headers->get('location');
        $client->request('GET', $articleLocation, [], [], $getHeaders, $content);
        $articleId = json_decode($client->getResponse()->getContent(), true)['id'];

        $content = <<<EOF
{"score": 6}
EOF;
        $client->request('POST', "/api/v1/articles/$articleId/ratings", [], [], $postHeaders, $content);
        $this->assertEquals(422, $client->getResponse()->getStatusCode());
    }



}
