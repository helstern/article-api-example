<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ArticleControllerTest extends WebTestCase
{
    public function testRetrieveArticleWithAnswers()
    {
        $postHeaders = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
        ];
        $getHeaders = ['HTTP_ACCEPT' => 'application/json'];
        $client = static::createClient();

        $content = <<<EOF
{"content": "article content", "author": "author1"}
EOF;
        $client->request('POST', '/api/v1/articles', [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $location = $client->getResponse()->headers->get('location');
        $client->request('GET', $location, [], [], $getHeaders);
        $articleId = json_decode($client->getResponse()->getContent(), true)['id'];


        for ($i = 1; $i <=3 ; $i++) {
            $content = <<<EOF
{"content": "answer $1"}
EOF;
            $client->request('POST', "/api/v1/articles/$articleId/answers", [], [], $postHeaders, $content);
        }

        $client->request('GET', $location, [], [], $getHeaders, $content);
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), 'failed to retrieve article');

        $decoded = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(3, count($decoded['answers']));
    }


    public function testCreateArticle()
    {
        $postHeaders = [
            'HTTP_ACCEPT' => 'application/json',
            'CONTENT_TYPE' => 'application/json; charset=UTF-8',
        ];
        $getHeaders = ['HTTP_ACCEPT' => 'application/json'];
        $client = static::createClient();

        $content = <<<EOF
{"content": "article content", "author": "author1"}
EOF;
        $client->request('POST', '/api/v1/articles', [], [], $postHeaders, $content);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $location = $client->getResponse()->headers->get('location');
        $client->request('GET', $location, [], [], $getHeaders);

        $client->request('GET', $location, [], [], $getHeaders, $content);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $decoded = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals([], $decoded['answers']);
    }

    public function testArticleNotFoundResponse()
    {
        $client = static::createClient();

        $server = array(
            'HTTP_ACCEPT' => 'application/json'
        );

        $client->request(
            'GET', '/api/v1/articles/99999',
            array(), array(),
            $server
        );

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
